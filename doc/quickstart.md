# Quickstart

- [Prerequisites](./prerequisites)

```
/home/tim/Music/%comment%/%genre%/%year%/%artist%/%album%/%track%-%title%.mp3
```

## Nutshell

1. Organise your MP3 collection into folders. The folders will become filters in **psalms** so it can be useful have more levels rather than less.
2. Add your settings.
3. Run the app.

## Step 1: Organise your MP3 collection

I use **puddletag** on linux for this, but you can use any mp3tagging software you like.

Assuming your collection is in the following folder: `/home/<user>/Music/`

Assuming you have your collection tagged!

- Using the "tag to filename" function, structure your MP3 collection. For instance:

```
/home/<user>/Music/Psalms/$mid(%year%, 2, 1)0s/%genre%/%year%/%artist%/%album%/%track% - %title%
```

Notice how I changed the path to a sub directory of `/home/user/Music/`? This will make it easier because puddletag does not remove empty folders. You'll want to remove all the folders inside the `Music` folder except `Psalms`. Then put all the folders in `Psalms` back into `Music`.

**psalms** gets all its data from the directory system. It doesn't read the MP3 tags. Album art should also be included in the folder using a consistent filename like `cover.jpg` or `folder.jpg`.

- Use the `Export Album Art to File` feature of **puddletag** or your MP3 tagger for album art.

## Step 2: psalms configuration

The app requires the following settings. Create a `psalms.cfg` file and put it somewhere outside of this repo. You could put it somewhere in your home directory, perhaps even with your music.

```
# /home/<user>/Music/psalms.cgf
SECRET_KEY="9&s6sGWrd%Sg82JNxnidw"
PSALMS_USER="psalms"
PSALMS_PASS="letmein"
PSALMS_PATH="/home/tim/repo//elio/eliofaithful/psalms/demomusic"
PATHWAYS="year/genre/artist/album/name"
SIDEBAR_FILTERING_ABOVE="artist"
BUCKETS="red,green,blue"
ALBUM_ART="folder.jpg"
```

### `SECRET_KEY`

Anything obscure - used by Flask.

### `PSALMS_USER` and `PSALMS_PASS`

Simple. The username and password you'll use to access the music on the webserver.

### `PSALMS_PATH`

The root folder where you keep your MP3 collection.

### `PATHWAYS`

Should exactly mask your folder structure. The names `artist`, `album`, `name` are required by **psalms** as these become the field names used in playlists.

In my example I have used the names `year` and `genre` to label the folders, but you can use any names you like.

I personally like to use `decade` as the highest level, e.g. `decade/genre/year/artist/album/name`. There are plenty of ways and reasons to pick a different layout.

For instance you could split the collection for different people, puttings all Sally's music into a super folder call `sally` and all Sid's music into a folder call `sid`. You would then create a `PATHWAYS` like this:

```
user/decade/genre/year/artist/album/name
```

**psalms** will automatically create a filter for each user in the sidebar.

Or you could split your music further into preferences by having a super folder called `best`, `okay`, `meh`. Sally could split her music into types like `party`, `hardday`, `bathtime`. `PATHWAYS` should look like this:

```
user/type/decade/genre/year/artist/album/name
```

### `SIDEBAR_FILTERING_ABOVE`

Filters for these folders (the PATHWAYS) are placed into the sidebar on the left. For instance, in the example above the filters sidebar would look like this:

```
user
[sally] [sid]

type
[best] [okay] [meh]

decade
[80s] [90s]

genre
[jazz] [rock]
```

If you changed `SIDEBAR_FILTERING_ABOVE` to `genre`, this would remove genre from that sidebar. If you changed it to `decade` that would also be removed from the filters sidebar.

The idea is to keep the sidebar as small as possible, as a way to quickly filter your music at a high level. It would not be a good idea to put `SIDEBAR_FILTERING_ABOVE` as `name` because otherwise your filters sidebar would be very long for a large MP3 collection. If you only have twenty or so artists, `album` might be a good choice. For most cases choose a `SIDEBAR_FILTERING_ABOVE` above `artist`.

The right side lists artists and albums, along with filters for all the other PATHWAYS. So if `SIDEBAR_FILTERING_ABOVE` = `genre` you'll still be able to filter by genre using the buttons on the right hand side.

The hard part is setting up your MP3 collection. **psalms** was purpose made for the developer who has always kept a very tidy, highly organised MP3 collection.

### `BUCKETS`

Buckets are **psalms** version of playlists. Whenever an album is playing, you can put it one or more buckets. You can create album collections this way. To filter all your music to a select few albums, click the "bucket" button in the sidebar bar. This should be a comma separated list.

### `ALBUM_ART`

The file name you use for your album art.

## Step 3: Running psalms

From now on it's easy. You can quickly run your app as follows:

```
cd psalms
virtualenv --python=python3 venv-psalms
source venv-psalms/bin/activate
# or
source venv-psalms/bin/activate.fish
pip install -r requirements/local.txt
export CONFIG_PATH="/home/user/Music/psalms.cfg"
gunicorn --bind 0.0.0.0:8090 wsgi:app
```

- USER: PSALMS_USER
- PASS: PSALMS_PASS

Navigate to [psalms](http://localhost:8090). Play your music. You can access this website from any computers on the same network, using the machines's IP address which you can get with the command `ifconfig`. e.g.:

<http://192.168.0.23:8090/>

## Step 4: Installing psalms as a service

You won't want to run **psalms** manually each time. More likely you'll set it up to run as a service on an old PC or a Raspberry PI.

Read the Installing page for more details about how to do this:

- [Installing psalms](./doc/installing)
