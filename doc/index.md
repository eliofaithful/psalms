# psalms

<figure>
  <img src="star.png" alt="">
</figure>

> Hear this, all ye people; give ear. Psalm 49 **the elioWay**

![alpha](//docs.elioway/./static/alpha.png "alpha")

**psalms** is a web-based, MP3 player for a personal,locally stored collection.

The key notes:

- Button driven.
- Single Page Application.
- Album focused.
- Plays music organised in directories. It does not read MP3 tags. The MP3 files and folders are DB.
- Flexible, configurable, with one click filtering to quickly find music to play.
- The server has a tiny code footprint. Very hackable.
- Written using the **Flask** microframework.
- Uses the [amplitudejs](https://521dimensions.com/open-source/amplitudejs) javascript plugin for playing music.
- Revolutionary "buckets" technology. Well, maybe not "revolutionary" so much as reactionary. There are definately "buckets".
- Rapsberry PI ready.

It's personally what I look for in an MP3 player. With it you can quickly find an album to play from within a large, well organised MP3 collection. Or you can modify it for your own purposes.

<div><a href="installing.html">
  <button>Installing</button>
</a>
  <a href="quickstart.html">
  <button>Quickstart</button>
</a>
  <a href="demo.html">
  <button>Demo</button>
</a></div>
