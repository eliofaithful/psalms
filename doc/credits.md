# psalms Credits

## Core, thanks!

- [Flask](http://flask.palletsprojects.com/en/1.1.x/)
- [pytest-flask](https://pytest-flask.readthedocs.io/en/latest/)
- [jinja2 templates](https://jinja.palletsprojects.com/en/2.10.x/templates/)
- [amplitudejs](https://521dimensions.com/open-source/amplitudejs)

## Artwork

- [vector.me:sixteenth_note](https://vector.me/browse/326394/sixteenth_note_stem_facing_up)
- [wikimedia:King_David](https://commons.wikimedia.org/wiki/File:King_David_by_Johann_Baptist_Moroder-Lusenberg_Urtij%C3%ABi.JPG)
- [peck](https://commons.wikimedia.org/wiki/File:Black_book_detective_193402.jpg)
- [fluidity](https://commons.wikimedia.org/wiki/File:Amniotic_fluid_aspiration.jpg)
- [sulky](https://commons.wikimedia.org/wiki/File:Casalini_Sulky_in_Italy.jpg)

## These were helpful

- [gist:DingGGu:mp3 streaming](https://gist.github.com/DingGGu/8144a2b96075deaf1bac)
- [github:jakecoffman:flask-tutorial](https://github.com/jakecoffman/flask-tutorial)
- [gist:malero:sort python dictionaries](https://gist.github.com/malero/418204/3afe18d1adfe5762dcad3c83b13a702291f0913a)
- [pythonise:flask-application-structure](https://pythonise.com/feed/flask/flask-application-structure)
- [serversideup:building-a-single-song-player](https://serversideup.net/building-a-single-song-player/)
- [github:html5-progress-bar](https://github.com/hongkiat/html5-progress-bar/blob/master/css/style.css)
- [codepen:JAGATHISH1123:scrollbar](https://codepen.io/JAGATHISH1123/pen/JqLjvx)
- [medium:faun:uwsgi-and-nginx](https://medium.com/faun/deploy-flask-app-with-nginx-using-gunicorn-7fda4f50066a)
- [simpleit:flask-with-dotenv](https://simpleit.rocks/python/flask/managing-environment-configuration-variables-in-flask-with-dotenv/)
- [damyanon.net:flask configuration](https://damyanon.net/post/flask-series-configuration/)
- [docs.python.org:howto:sorting](https://docs.python.org/3/howto/sorting.html)

## Maybe:

- <https://github.com/goldfire/howler.js>
- <https://modernweb.com/develop-an-ios-application-with-node-js-and-cordova/>
