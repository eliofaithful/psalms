# Installing psalms

- [Prerequisites](./prerequisites)

## Installing

### Download and install psalms

Clone the repo into a folder somewhere on your computer - your home directory is fine.

```
cd /home/<user>
git clone https://gitlab.com/eliofaithful/psalms.git
cd psalms
```

Setup a virtualenv for the app:

```
virtualenv --python=python3 venv-psalms
# or
python3 -m venv venv-psalms

source venv-psalms/bin/activate
# or
source venv-psalms/bin/activate.fish
```

Install psalms + requirements

```
pip install -r requirements/local.txt
```

Check it works with the demo music:

```
./run.py
```

### Configuration

Create a `psalms.cfg` file and put it somewhere outside of this repo. You could put it somewhere in your home directory, perhaps even with your music. Details of these settings are described in the [psalms Quickstart](//docs.elioway/./eliofaithful/psalms/doc/quickstart) along with infomation on setting up your MP3 music collection.

```
# /home/<user>/Music/psalms.cgf
SECRET_KEY="piodcntypbw4e6tcybphxnaniotypq4t9v4h"
PSALMS_USER="colaboy"
PSALMS_PASS="hays-sonic-summon-kitten"
PSALMS_PATH="/home/tim/Music"
PATHWAYS="decade/genre/year/artist/album/name"
SIDEBAR_FILTERING_ABOVE="year"
BUCKETS="pink,plum,lime,gold,blue,gray,fix"
ALBUM_ART="folder.jpg"
```

- Create a `.env` or `.private` file. It needs just one setting - `CONFIG_PATH` - which will point to the config file described above. I suggest putting the two files in the root of your MP3 collection. e.g. `/home/<user>/Music/.private`

```
CONFIG_PATH=/home/<user>/Music/psalms.cfg
```

### Create a Gunicorn Service

- Edit the paths in the file `installers/psalms.service` relative to the **psalms** repo you cloned, e.g.:

  - `WorkingDirectory=/home/<user>/psalms`
  - `Environment="PATH=/home/<user>/psalms/venv-psalms/bin"`
  - `EnvironmentFile=/home/tim/Music/.private`
  - `ExecStart=/home/<user>/psalms/venv-psalms/bin/gunicorn --workers 3 --bind unix:psalms.sock -m 007 wsgi:app`

- Run the following commands.

```shell
# sudo rm /etc/systemd/system/installers/psalms.service
sudo cp installers/psalms.service /etc/systemd/system/

sudo systemctl start psalms
sudo systemctl enable psalms
```

Status check.

```shell
sudo systemctl status psalms
```

Issues:

```shell
sudo systemctl daemon-reload
sudo systemctl start psalms
```

### Nginx setup

- Find out what your machine's IP address is using `ifconfig`. It will usually start `192.168.`, otherwise you've set things up differently on this machine and you'll probably know what it is without my help.

- Edit the paths in the file `installers/psalms`, e.g.:

  - `server_name <IP ADDRESS>;`
  - `proxy_pass http://unix://home/<user>/psalms/psalms.sock;`

- Run the following commands.

```shell
sudo rm /etc/nginx/sites-enabled/psalms
sudo cp installers/psalms /etc/nginx/sites-available/psalms
sudo ln -s /etc/nginx/sites-available/psalms /etc/nginx/sites-enabled
sudo systemctl restart nginx
sudo ufw allow 'Nginx Full'
```

Status check.

```shell
sudo nginx -t
```

Issues:

- Apache home page showing? Remove the default site.

```
sudo rm /etc/nginx/sites-enabled/default
```

More:

<https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-in-ubuntu-18-04>

### Testing your installation

Reboot your machine and navigate to <http://IPADDRESS>, i.e. the IP of the machine you installed **psalms** into.

Once you have the application running on your server, you should read the [psalms Quickstart](//docs.elioway/./eliofaithful/psalms/doc/quickstart) which has details on how to set up your MP3 collection ready for **psalms**.

## Contributing

```shell
cd elioway
git clone https://gitlab.com/elioway/eliofaithful.git
cd eliofaithful
git clone https://gitlab.com/eliofaithful/psalms.git
cd psalms
```

**Setup**

```
virtualenv --python=python3 venv-psalms
# or
python3 -m venv venv-psalms

source .env
# or
source .env.fish
pip install -r requirements/local.txt
```

**Test**

```
pytest
```

**Run**

```
./run.py
```

**Clean code**

```
black .
```

Now add some great new features! See [Psalms Issues](https://gitlab.com/eliofaithful/psalms/issues) for any outstanding tasks you can help with.
