# Primer

## Python Lists

<https://docs.python.org/3/howto/sorting.html>

```
from operator import itemgetter, attrgetter

sorted("This is a test string from Andrew".split(), key=str.lower)
["a", "Andrew", "from", "is", "string", "test", "This"]

student_tuples = [
    ("john", "A", 15),
    ("jane", "B", 12),
    ("dave", "B", 10),
]
sorted(student_tuples, key=lambda student: student[2])  # sort by age
sorted(student_tuples, key=itemgetter(2))
sorted(student_tuples, key=itemgetter(1, 2), reverse=True)
[("dave", "B", 10), ("jane", "B", 12), ("john", "A", 15)]

student_dict_list = [
    {"name": "john", "grade": "A", "age": 15},
    {"name": "jane", "grade": "B", "age": 12},
    {"name": "dave", "grade": "B", "age": 10},
]
sorted(student_tuples, key=lambda student: student["age"])  # sort by age
sorted(student_tuples, key=attrgetter("age"))
sorted(student_tuples, key=attrgetter("grade", "age"))
[("dave", "B", 10), ("jane", "B", 12), ("john", "A", 15)]


class Student:
    def __init__(self, name, grade, age):
        self.name = name
        self.grade = grade
        self.age = age

    def __repr__(self):
        return repr((self.name, self.grade, self.age))


student_objects = [
    Student("john", "A", 15),
    Student("jane", "B", 12),
    Student("dave", "B", 10),
]
sorted(student_objects, key=lambda student: student.age)  # sort by age
sorted(student_objects, key=attrgetter("age"))
sorted(student_objects, key=attrgetter("grade", "age"))
[("dave", "B", 10), ("jane", "B", 12), ("john", "A", 15)]
```

### DECORATE-SORT-UNDECORATE

```
decorated = [(student.grade, i, student) for i, student in enumerate(student_objects)]
decorated.sort()
[student for grade, i, student in decorated]               # undecorate
[('john', 'A', 15), ('jane', 'B', 12), ('dave', 'B', 10)]
```

### USING THE CMP PARAMETER

```
def numeric_compare(x, y):
    return x - y
sorted([5, 2, 4, 1, 3], cmp=numeric_compare)


def reverse_numeric(x, y):
    return y - x
sorted([5, 2, 4, 1, 3], cmp=reverse_numeric)
```

### USING THE cmp_to_key FUNCTION

Situation can arise when you have the user supplying a comparison function and you need to convert that to a key function. The following wrapper makes that easy to do:

```
def cmp_to_key(mycmp):
    'Convert a cmp= function into a key= function'
    class K:
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0
        def __gt__(self, other):
            return mycmp(self.obj, other.obj) > 0
        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0
        def __le__(self, other):
            return mycmp(self.obj, other.obj) <= 0
        def __ge__(self, other):
            return mycmp(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, other.obj) != 0
    return K
sorted([5, 2, 4, 1, 3], key=cmp_to_key(reverse_numeric))
```

### SORT A LIST ACCORDING TO ANOTHER LIST

Shortest Code

```
X = ["a", "b", "c", "d", "e", "f", "g", "h", "i"]
Y = [ 0,   1,   1,    0,   1,   2,   2,   0,   1]
[x for _,x in sorted(zip(Y, X))]
["a", "d", "h", "b", "c", "e", "i", "f", "g"]
```

Generally Speaking

```
[x for _, x in sorted(zip(Y,X), key=lambda pair: pair[0])]
```

### SORT A LIST ACCORDING TO PRESET VALUE

```
from operator import itemgetter
RANK = {"red": 100, "green": 70, "yellow": 30, "blue": 0}
LIST = ["yellow", "blue", "red", "red"]
sort_as = [(col, RANK.get(col, 0)) for col in LIST]
[col for col, rank in sorted(sort_as, key=itemgetter(1))]
["blue", "yellow", "red", "red"]
```
