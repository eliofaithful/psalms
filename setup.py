# -*- coding: utf-8 -*-
import os
import setuptools
from distutils.core import setup


def read_file_into_string(filename):
    path = os.path.abspath(os.path.dirname(__file__))
    filepath = os.path.join(path, filename)
    try:
        return open(filepath).read()
    except IOError:
        return ""


def get_readme():
    if os.path.exists("README.md"):
        return read_file_into_string("README.md")
    return ""


def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join("..", path, filename))
    return paths


mp3_demo_files = package_files("demomusic")
mp3_test_demo_files = package_files("psalms/tests/testmusic")

setup(
    name="psalms",
    packages=setuptools.find_packages(),
    package_data={
        "demomusic": mp3_demo_files,
        "docs": ["doc/*.*"],
        "installers": ["installers/*.*"],
        "psalms": [
            "psalms/static/*.*",
            "psalms/static/css/*.*",
            "psalms/static/icons/*.*",
            "psalms/static/js/*.*",
            "psalms/templates/*.*",
        ]
        + mp3_test_demo_files,
    },
    install_requires=["Flask", "gunicorn", "uwsgi"],
    version="1.0",
    description="Web-based MP3 player for your personal music collection.",
    url="https://elioway.gitlab.io/eliofaithful/psalms",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Web Environment",
        "Framework :: Flask",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Server",
        "Topic :: Multimedia :: Sound/Audio :: Players :: MP3",
    ],
    author="Tim Bushell",
    author_email="tcbushell@gmail.com",
    license="MIT",
    include_package_data=True,
    zip_safe=False,
    long_description=get_readme(),
    long_description_content_type="text/markdown",
)
