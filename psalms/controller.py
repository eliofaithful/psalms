# -*- coding: utf-8 -*-
import flask
from psalms.model import all_music, is_in_db, remove, results, sidebar
from psalms.utils import multikeysort, set_sort_columns, PickleJar

import logging

logger = logging.getLogger(__name__)
logging.basicConfig(
    level=logging.NOTSET,
    format='%(asctime)s %(levelname)-8s %(message)s',
    datefmt='%a, %d %b %Y %H:%M:%S',
    filename='/home/tim/Music/logger.log',
    filemode='w'
)



def get_pickle(bucket):
    PSALMS_PATH = flask.current_app.config["PSALMS_PATH"]
    PSALMS_USER = flask.current_app.config["PSALMS_USER"]
    return PickleJar(f"{PSALMS_PATH}/buckets", bucket)


def get_bucket_db(bucket):
    # A list of buckets you can use to create album lists.
    BUCKETS = flask.current_app.config["BUCKETS"].split(",")
    bucket_pj = get_pickle(bucket)
    bucket_db = bucket_pj.open([])
    # The "all" bucket is all music and should be populated at least once
    if not bucket_db and bucket == "all":
        # .. but only if there is nothing in the buckets
        for buck in BUCKETS:
            check_db = get_bucket_db(buck)
            if check_db:
                return bucket_db
        # Now populate the "all" bucket
        PATHWAYS = flask.current_app.config["PATHWAYS"]
        PSALMS_PATH = flask.current_app.config["PSALMS_PATH"]
        bucket_db = all_music(PSALMS_PATH, PATHWAYS)
        bucket_pj.pickle(bucket_db)
    return bucket_db


def get_album_buckets(album):
    # A list of buckets you can use to create album lists.
    BUCKETS = flask.current_app.config["BUCKETS"].split(",") + ["recent"]
    # Return a list of all the buckets this album is in.
    album_in_buckets = []
    for buck in BUCKETS:
        bucket_db = get_bucket_db(buck)
        if is_in_db(bucket_db, {"album": album}):
            album_in_buckets.append(buck)
    return album_in_buckets


def bucket_toggle(bucket, album):
    # Get what buckets this album is already in (it may be 0 or 5).
    album_buckets = get_album_buckets(album)
    logger.info(f"{bucket} {album} get_album_buckets in {album_buckets}")
    # A list of buckets you can use to create album lists.
    BUCKETS = flask.current_app.config["BUCKETS"].split(",")
    # Only put the album in buckets found in settings (or recent).
    if not bucket in BUCKETS:
        return album_buckets
    # Put it in recent once only.
    if bucket == "recent" and "recent" in album_buckets:
        return album_buckets
    # Get the album tracks from one of the other buckets it's in or "all".
    # Target may be the incoming bucket.
    get_from_target_bucket = album_buckets + ["all"]
    target_pj = get_pickle(get_from_target_bucket[0])
    target_db = target_pj.open([])
    album_results = results(target_db, {"album": album})
    # Get the bucket in question.
    bucket_pj = get_pickle(bucket)
    bucket_db = bucket_pj.open([])
    # If the album is already in the bucket, REMOVE it.
    if bucket in album_buckets:
        bucket_db = remove(bucket_db, {"album": album})
        bucket_pj.pickle(bucket_db)
        album_buckets.remove(bucket)
    else:
        # ... if not in the bucket, ADD it.
        bucket_pj.pickle(bucket_db + album_results)
        album_buckets.append(bucket)
    # An album can be in more than one bucket, but never in a bucket and "all".
    if get_from_target_bucket[0] == "all":
        # Remove from the "all" database.
        target_pj.pickle(remove(target_db, {"album": album}))
    # An album should be in "all" if it's not in any bucket.
    if len(album_buckets) == 0:
        all_pj = get_pickle("all")
        all_db = all_pj.open([])
        all_pj.pickle(all_db + album_results)
    return album_buckets


def navigator(bucket, filters={}):
    # A "map" of the mp3 collection's directory structure used as keys for filter buttons.
    PATHWAYS = flask.current_app.config["PATHWAYS"]
    # Where we split the "map" for permanent filters on the left.
    SIDEBAR_FILTERING_ABOVE = flask.current_app.config[
        "SIDEBAR_FILTERING_ABOVE"
    ]
    # try and get a record of the database from a pickle
    json_db = get_bucket_db(bucket)
    sidebar_links = sidebar(json_db, PATHWAYS, SIDEBAR_FILTERING_ABOVE)
    results_db = multikeysort(
        results(json_db, filters),
        set_sort_columns(PATHWAYS, SIDEBAR_FILTERING_ABOVE),
    )
    return sidebar_links, results_db
