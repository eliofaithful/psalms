# https://gist.github.com/malero/418204/3afe18d1adfe5762dcad3c83b13a702291f0913a#gistcomment-2266646
# -*- coding: utf-8 -*-
import os
import pickle
from functools import cmp_to_key
from operator import itemgetter, methodcaller


def filter_or(item, filter):
    """Return True if matching fields two dictionaries have any same values."""
    if not item or not filter:
        return False
    # Convert both to sets: See if there is an intersection of anything.
    return set(item.items()).intersection(set(filter.items()))


def filter_and(item, filter):
    """Return True if matching fields two dictionaries have the same values."""
    if not item or not filter:
        return False
    # Convert both to sets: See if the intersection is the same as the filter.
    return set(item.items()).intersection(set(filter.items())) == set(
        filter.items()
    )


def cmp(a, b):
    try:
        return (a > b) - (a < b)
    except TypeError:
        return -1


def multikeysort(items, columns, functions={}, getter=itemgetter):
    """Sort a list of dictionary objects or objects by multiple keys bidirectionally.

    Keyword Arguments:
    items -- A list of dictionary objects or objects
    columns -- A list of column names to sort by. Use -column to sort in descending order
    functions -- A Dictionary of Column Name -> Functions to normalize or process each column value
    getter -- Default "getter" if column function does not exist
              operator.itemgetter for Dictionaries
              operator.attrgetter for Objects
    """
    comparers = []
    for col in columns:
        column = col[1:] if col.startswith("-") else col
        if not column in functions:
            functions[column] = getter(column)
        comparers.append((functions[column], 1 if column == col else -1))

    def comparer(left, right):
        for func, polarity in comparers:
            result = cmp(func(left), func(right))
            if result:
                return polarity * result
        else:
            return 0

    return sorted(items, key=cmp_to_key(comparer))


def compose(inner_func, *outer_funcs):
    """Compose multiple unary functions together into a single unary function"""
    if not outer_funcs:
        return inner_func
    outer_func = compose(*outer_funcs)
    return lambda *args, **kwargs: outer_func(inner_func(*args, **kwargs))


def set_sort_columns(pathways, filter_from):
    pathways_split = pathways.split("/")
    filter_from_index = pathways_split.index(filter_from)
    return (
        pathways_split[filter_from_index:] + pathways_split[:filter_from_index]
    )


class PickleJar:
    """A convenient wrapper for pickling data, handling os/read/write"""

    """**Usage**::

        from picklejar import PickleJar
        data = ['Mango'] # or {} or ''
        chutney_pj = PickleJar('projectpath/appfolder/optionalfolder', 'cucumber')
        chutney_pj.pickle(data)

        # Muck about with the current data.
        data = chutney_pj.open([])
        data += ['Clove', 'Cinnamon', 'Pepper']

        # Repickle it.
        chutney_pj.pickle(data)"""

    DEFAULT = {}

    def __init__(self, jarpath, jarname):
        """Set path names to the picklejar by name."""
        self.jarname = jarname
        self.jarpath = jarpath
        if not os.path.exists(self.jarpath):
            os.makedirs(self.jarpath, exist_ok=True)
        self.jarfile = f"{jarname}.pickle"
        self.picklejar = os.path.join(self.jarpath, self.jarfile)

    @property
    def ripe(self):
        """Check for picklejar."""
        return os.path.exists(self.picklejar)

    def wipe(self):
        """Wipe old."""
        if self.ripe:
            os.remove(self.picklejar)

    def pickle(self, data):
        """Store the data."""
        with open(self.picklejar, "wb") as fp:
            pickle.dump(data, fp)
        return data

    def open(self, default=DEFAULT):
        """Read the data."""
        if not self.ripe:
            return self.pickle(default)
        with open(self.picklejar, "rb") as fp:
            return pickle.load(fp)
