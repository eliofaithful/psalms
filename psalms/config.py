# -*- coding: utf-8 -*-
import os


class BaseConfig(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = "abcdef!£$^&0123456789"
    PSALMS_USER = "psalms"
    PSALMS_PASS = "letmein"
    PSALMS_PATH = "./demomusic"
    PATHWAYS = "year/genre/artist/album/name"
    SIDEBAR_FILTERING_ABOVE = "artist"
    ALBUM_ART = "folder.jpg"
    BUCKETS = "e,l,i,o"


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = True


class TestingConfig(BaseConfig):
    DEBUG = False
    TESTING = True
