import flask
import os
from psalms.jinja_filters import qs_active, qs_toggler
from psalms.views import Buckets, Main, Music

config_objects = {
    "development": "psalms.config.DevelopmentConfig",
    "testing": "psalms.config.TestingConfig",
    "default": "psalms.config.BaseConfig",
}

# You just use two optional env variables.
CONFIG_TYPE = os.getenv("CONFIG_TYPE", "default")
CONFIG_PATH = os.getenv("CONFIG_PATH", "./demomusic/psalms.cfg")

default_config_path = os.path.abspath(CONFIG_PATH)
print("default_config_path", default_config_path)

# Doing it the application factory way.
def create_app(config_path=default_config_path):

    app = flask.Flask(__name__)
    # Get the default settings (with the demo MP3 collection).
    app.config.from_object(config_objects[CONFIG_TYPE])
    # Get the custom settings.
    app.config.from_pyfile(config_path)

    SECRET_KEY = app.config["SECRET_KEY"]
    if not SECRET_KEY:
        raise ValueError("No SECRET_KEY set for Flask application")

    app.secret_key = SECRET_KEY
    app.debug = bool(app.config["DEBUG"])

    # Templating
    app.jinja_env.filters["qs_active"] = qs_active
    app.jinja_env.filters["qs_toggler"] = qs_toggler
    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True

    # Urls
    app.add_url_rule(
        "/", view_func=Main.as_view("index"), methods=["GET", "POST"]
    )
    app.add_url_rule(
        "/buckets/",
        view_func=Buckets.as_view("buckets"),
        methods=["GET", "POST"],
    )
    for bucket in app.config["BUCKETS"].split(",") + ["all", "recent"]:
        app.add_url_rule(
            f"/{bucket}/", view_func=Music.as_view(bucket), methods=["GET"]
        )

    # Special view for sending the MP3 file and folder.jpg from outside of the app.
    @app.route("/mp3s/<path:filename>")
    def download_file(filename):
        return flask.send_from_directory(
            os.path.abspath(app.config["PSALMS_PATH"]), filename
        )

    # Return the app for our factory.
    return app
