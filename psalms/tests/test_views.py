import json
from flask import url_for, session, request
from psalms.tests.cases import LoginCase


class TestViews(LoginCase):
    def test_login(self):
        response = self.login("hacker", "testpass")
        assert response.status_code == 200
        assert (
            b"Username doesn&#39;t exist or incorrect password" in response.data
        )

        response = self.login("testuser", "hacker")
        assert response.status_code == 200
        assert (
            b"Username doesn&#39;t exist or incorrect password" in response.data
        )

        response = self.login("testuser", "testpass")

        assert response.status_code == 200
        assert b"<b>testuser</b>" in response.data
        assert "testuser" == session["username"]

        response = self.logout()

        assert response.status_code == 200
        assert b'<input type="text" name="username"/>' in response.data
        assert "notinsession" == session.get("username", "notinsession")

    def test_must_login_to_access_music(self):
        response = self.client.get(url_for("all"))
        assert response.status_code == 302

        response = self.client.get(url_for("all"), follow_redirects=True)
        assert response.status_code == 200
        assert "http://localhost/" == request.url

    def test_all_music(self):
        self.login("testuser", "testpass")
        response = self.client.get(url_for("all"))
        assert b"02 - pecks reprise" in response.data
        assert b'<a href="/all/?genre=acidjazz">' in response.data

    def test_music_bucket(self):
        self.login("testuser", "testpass")

        # Album not in bucket1
        response = self.client.get(url_for("bucket1"))
        assert not b"02 - pecks reprise" in response.data
        assert not b'<a href="/bucket1/?genre=jazz">' in response.data

        # Album in all
        response = self.client.get(url_for("all"))
        assert b"02 - pecks reprise" in response.data
        assert b'<a href="/all/?genre=jazz">' in response.data

        self.client.post(
            url_for("buckets"),
            data=dict(bucket="bucket1", album="peck"),
            follow_redirects=True,
        )

        # Album in bucket1
        response = self.client.get(url_for("bucket1"))
        assert b"02 - pecks reprise" in response.data
        assert b'<a href="/bucket1/?genre=jazz">' in response.data

        # Album not in all
        response = self.client.get(url_for("all"))
        assert not b"02 - pecks reprise" in response.data
        assert not b'<a href="/all/?genre=jazz">' in response.data

    def test_buckets(self):
        self.login("testuser", "testpass")
        response = self.client.post(
            url_for("buckets"),
            data=dict(bucket="bucket1", album="peck"),
            follow_redirects=True,
        )
        assert {"buckets": ["bucket1"]} == response.json

        response = self.client.get(
            "{}?album=peck".format(url_for("buckets")), follow_redirects=True
        )
        assert {"buckets": ["bucket1"]} == response.json

        response = self.client.post(
            url_for("buckets"),
            data=dict(bucket="bucket2", album="peck"),
            follow_redirects=True,
        )
        assert {"buckets": ["bucket1", "bucket2"]} == response.json

    def test_download_file(self):
        self.login("testuser", "testpass")
        response = self.client.get("/mp3s/1998/jazz/colaboy/peck/folder.jpg")
        assert "/mp3s/1998/jazz/colaboy/peck/folder.jpg" in request.url
