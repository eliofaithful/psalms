# -*- coding: utf-8 -*-
import os
import pytest
import shutil
import unittest
from flask import url_for
from psalms.controller import get_bucket_db


@pytest.mark.usefixtures("client_class")
class BucketCase(unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.tearDown()
        # build the "all" db at the start of each test
        get_bucket_db("all")

    def tearDown(self):
        super().tearDown()
        testuser_buckets = os.path.join(
            os.path.abspath("."), "psalms/tests/testmusic/buckets"
        )
        shutil.rmtree(testuser_buckets, ignore_errors=True, onerror=None)


class LoginCase(BucketCase):
    def login(self, username, password):
        return self.client.post(
            url_for("index"),
            data=dict(username=username, passwd=password),
            follow_redirects=True,
        )

    def logout(self):
        return self.client.post(
            url_for("index"), data=dict(logout="logout"), follow_redirects=True
        )


full_results = [
    {
        "art": "1998/jazz/colaboy/peck/folder.jpg",
        "url": "1998/jazz/colaboy/peck/01%20-%20pecks%20theme.mp3",
        "year": "1998",
        "genre": "jazz",
        "artist": "colaboy",
        "album": "peck",
        "name": "01 - pecks theme",
    },
    {
        "art": "1998/jazz/colaboy/peck/folder.jpg",
        "url": "1998/jazz/colaboy/peck/02%20-%20pecks%20reprise.mp3",
        "year": "1998",
        "genre": "jazz",
        "artist": "colaboy",
        "album": "peck",
        "name": "02 - pecks reprise",
    },
    {
        "art": "2001/classical/elioway/fluidity/folder.jpg",
        "url": "2001/classical/elioway/fluidity/01%20-%20fluidity%20-%20part%20i.mp3",
        "year": "2001",
        "genre": "classical",
        "artist": "elioway",
        "album": "fluidity",
        "name": "01 - fluidity - part i",
    },
    {
        "art": "2001/classical/elioway/fluidity/folder.jpg",
        "url": "2001/classical/elioway/fluidity/02%20-%20fluidity%20-%20part%20ii.mp3",
        "year": "2001",
        "genre": "classical",
        "artist": "elioway",
        "album": "fluidity",
        "name": "02 - fluidity - part ii",
    },
    {
        "art": "2000/acidjazz/tim%20bushell/sulky/folder.jpg",
        "url": "2000/acidjazz/tim%20bushell/sulky/01%20-%20sulky.mp3",
        "year": "2000",
        "genre": "acidjazz",
        "artist": "tim bushell",
        "album": "sulky",
        "name": "01 - sulky",
    },
    {
        "art": "2000/acidjazz/tim%20bushell/sulky/folder.jpg",
        "url": "2000/acidjazz/tim%20bushell/sulky/02%20-%20nervegas.mp3",
        "year": "2000",
        "genre": "acidjazz",
        "artist": "tim bushell",
        "album": "sulky",
        "name": "02 - nervegas",
    },
    {
        "art": "2000/acidjazz/tim%20bushell/sulky/folder.jpg",
        "url": "2000/acidjazz/tim%20bushell/sulky/03%20-%20electric%20daze.mp3",
        "year": "2000",
        "genre": "acidjazz",
        "artist": "tim bushell",
        "album": "sulky",
        "name": "03 - electric daze",
    },
    {
        "art": "2000/classical/tim%20bushell/untitled/folder.jpg",
        "url": "2000/classical/tim%20bushell/untitled/01%20-%20untitled.mp3",
        "year": "2000",
        "genre": "classical",
        "artist": "tim bushell",
        "album": "untitled",
        "name": "01 - untitled",
    },
]
