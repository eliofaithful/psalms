# -*- coding: utf-8 -*-
import unittest
from psalms.utils import filter_and, filter_or, multikeysort, set_sort_columns
from .test_model import json_db


class TestUtilsFilters(unittest.TestCase):
    def test_utils_filter_or(self):
        self.assertTrue(filter_or({"a": 1, "b": 2}, {"a": 1, "b": 2}))
        self.assertTrue(filter_or({"a": 1, "b": 2, "c": 3}, {"a": 1, "b": 2}))
        self.assertTrue(filter_or({"a": 1, "b": 2}, {"a": 2, "b": 2}))
        self.assertTrue(filter_or({"a": 1, "b": 2}, {"a": 1, "b": 1}))
        self.assertTrue(filter_or({"a": 1, "b": 2, "c": 3}, {"a": 1, "b": 2}))
        self.assertFalse(filter_or({"a": 1, "b": 2, "c": 3}, {"a": 4, "b": 5}))
        self.assertFalse(filter_or({"a": 1, "b": 2}, {"a": 2, "b": 1}))
        self.assertFalse(filter_or({}, {"a": 1, "b": 2}))
        self.assertFalse(filter_or({"a": 1, "b": 2}, {}))

    def test_utils_filter_and(self):
        self.assertTrue(filter_and({"a": 1, "b": 2}, {"a": 1, "b": 2}))
        self.assertTrue(filter_and({"a": 1, "b": 2, "c": 3}, {"a": 1, "b": 2}))
        self.assertFalse(filter_and({"a": 1, "b": 2}, {"a": 2, "b": 2}))
        self.assertFalse(filter_and({"a": 1, "b": 2}, {"a": 1, "b": 1}))
        self.assertFalse(filter_and({"a": 1, "b": 2}, {"a": 1, "b": 2, "c": 3}))
        self.assertFalse(filter_and({}, {"a": 1, "b": 2}))
        self.assertFalse(filter_and({"a": 1, "b": 2}, {}))


class TestUtilsSortColumns(unittest.TestCase):
    def test_utils_set_sort_columns(self):
        self.assertListEqual(
            ["art", "alb", "trk", "f1", "f2"],
            set_sort_columns("f1/f2/art/alb/trk", "art"),
        )
        self.assertListEqual(
            ["g", "h", "i", "a", "b", "c", "d", "e", "f"],
            set_sort_columns("a/b/c/d/e/f/g/h/i", "g"),
        )
        self.assertListEqual(
            ["e", "f", "g", "h", "i", "a", "b", "c", "d"],
            set_sort_columns("a/b/c/d/e/f/g/h/i", "e"),
        )
        self.assertListEqual(
            ["a", "b", "c", "d", "e", "f", "g", "h", "i"],
            set_sort_columns("a/b/c/d/e/f/g/h/i", "a"),
        )
        self.assertListEqual(
            ["i", "a", "b", "c", "d", "e", "f", "g", "h"],
            set_sort_columns("a/b/c/d/e/f/g/h/i", "i"),
        )


class TestUtilsMultiColumnSort(unittest.TestCase):
    def test_utils_multikeysort(self):
        self.assertListEqual(
            json_db,
            multikeysort(
                [
                    json_db[mixed]
                    for mixed in [9, 5, 2, 6, 8, 3, 7, 11, 4, 10, 1, 0]
                ],
                set_sort_columns("f1/f2/art/alb/trk", "art"),
            ),
        )
        self.assertListEqual(
            json_db,
            multikeysort(
                [
                    json_db[mixed]
                    for mixed in [3, 7, 11, 4, 0, 1, 2, 9, 5, 6, 10, 8]
                ],
                set_sort_columns("f1/f2/art/alb/trk", "art"),
            ),
        )
