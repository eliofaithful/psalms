import os
import pytest
from psalms import create_app


@pytest.fixture
def app():
    app = create_app(config_path="./tests/test.cfg")
    app.config["PSALMS_PATH"] = os.path.join(
        os.path.abspath("."), "psalms/tests/testmusic"
    )
    return app
