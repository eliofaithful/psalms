# -*- coding: utf-8 -*-
import unittest
from psalms.model import is_in_db, mp3_model, remove, results, sidebar
from psalms.utils import multikeysort, set_sort_columns


pathways = "f1/f2/art/alb/trk"
sort_columns = set_sort_columns(pathways, "art")
x1_A1_01A1 = {"f1": "x", "f2": "1", "art": "A", "alb": "A1", "trk": "01A1"}
x1_A1_02A1 = {"f1": "x", "f2": "1", "art": "A", "alb": "A1", "trk": "02A1"}
x1_A1_03A1 = {"f1": "x", "f2": "1", "art": "A", "alb": "A1", "trk": "03A1"}
y2_A2_01A2 = {"f1": "y", "f2": "2", "art": "A", "alb": "A2", "trk": "01A2"}
y2_A2_02A2 = {"f1": "y", "f2": "2", "art": "A", "alb": "A2", "trk": "02A2"}
y3_A3_01A3 = {"f1": "y", "f2": "3", "art": "A", "alb": "A3", "trk": "01A3"}
y3_A3_02A3 = {"f1": "y", "f2": "3", "art": "A", "alb": "A3", "trk": "02A3"}
x2_B1_01B1 = {"f1": "x", "f2": "2", "art": "B", "alb": "B1", "trk": "01B1"}
x2_B1_02B1 = {"f1": "x", "f2": "2", "art": "B", "alb": "B1", "trk": "02B1"}
x2_B1_03B1 = {"f1": "x", "f2": "2", "art": "B", "alb": "B1", "trk": "03B1"}
y4_B2_01B2 = {"f1": "y", "f2": "4", "art": "B", "alb": "B2", "trk": "01B2"}
y4_B2_02B2 = {"f1": "y", "f2": "4", "art": "B", "alb": "B2", "trk": "02B2"}

json_db = [
    x1_A1_01A1,
    x1_A1_02A1,
    x1_A1_03A1,
    y2_A2_01A2,
    y2_A2_02A2,
    y3_A3_01A3,
    y3_A3_02A3,
    x2_B1_01B1,
    x2_B1_02B1,
    x2_B1_03B1,
    y4_B2_01B2,
    y4_B2_02B2,
]


class TestModelMp3Model(unittest.TestCase):
    def test_model_mp3_model_timitee_setup(self):
        self.assertDictEqual(
            {
                "year": "1967",
                "genre": "jazz",
                "artist": "miles",
                "album": "blues",
                "name": "02 blue",
                "art": "1967/jazz/miles/blues/folder.jpg",
                "url": "1967/jazz/miles/blues/02%20blue.mp3",
            },
            mp3_model(
                "/home/psalms/Music/1967/jazz/miles/blues/02 blue.mp3",
                "year/genre/artist/album/name",
            ),
        )

    def test_model_mp3_model_basic_setup(self):
        self.assertDictEqual(
            {
                "artist": "miles",
                "album": "blues",
                "name": "02 blue",
                "art": "miles/blues/folder.jpg",
                "url": "miles/blues/02%20blue.mp3",
            },
            mp3_model(
                "/home/psalms/My/Music/miles/blues/02 blue.mp3",
                "artist/album/name",
            ),
        )

    def test_model_mp3_model_deeply_nested_setup(self):
        self.assertDictEqual(
            {
                "a": "A",
                "b": "B",
                "c": "C",
                "d": "D",
                "e": "E",
                "f": "F",
                "g": "G",
                "h": "H.mp3",
                "art": "A/B/C/D/E/F/G/folder.jpg",
                "url": "A/B/C/D/E/F/G/H.mp3",
            },
            mp3_model("/1/2/3/4/Music/A/B/C/D/E/F/G/H.mp3", "a/b/c/d/e/f/g/h"),
        )


class TestSidebar(unittest.TestCase):
    def test_model_sidebar_f1(self):
        self.assertDictEqual({}, sidebar(json_db, pathways, "f1"))

    def test_model_sidebar_f2(self):
        self.assertDictEqual(
            {"f1": ["x", "y"]}, sidebar(json_db, pathways, "f2")
        )

    def test_model_sidebar_art(self):
        self.assertDictEqual(
            {"f1": ["x", "y"], "f2": ["1", "2", "3", "4"]},
            sidebar(json_db, pathways, "art"),
        )

    def test_model_sidebar_alb(self):
        self.assertDictEqual(
            {"f1": ["x", "y"], "f2": ["1", "2", "3", "4"], "art": ["A", "B"]},
            sidebar(json_db, pathways, "alb"),
        )

    def test_model_sidebar_trk(self):
        self.assertDictEqual(
            {
                "f1": ["x", "y"],
                "f2": ["1", "2", "3", "4"],
                "art": ["A", "B"],
                "alb": ["A1", "A2", "A3", "B1", "B2"],
            },
            sidebar(json_db, pathways, "trk"),
        )


class TestModelResults(unittest.TestCase):
    def test_model_results_by_no_active_filter(self):
        self.assertListEqual(
            [x1_A1_01A1, y2_A2_01A2, y3_A3_01A3, x2_B1_01B1, y4_B2_01B2],
            multikeysort(
                results(
                    [x1_A1_01A1, y2_A2_01A2, y3_A3_01A3, x2_B1_01B1, y4_B2_01B2]
                ),
                sort_columns,
            ),
        )
        self.assertListEqual(
            [x1_A1_01A1, y2_A2_01A2, y3_A3_01A3, x2_B1_01B1, y4_B2_01B2],
            multikeysort(
                results(
                    [
                        x1_A1_01A1,
                        y2_A2_01A2,
                        y3_A3_01A3,
                        x2_B1_01B1,
                        y4_B2_01B2,
                    ],
                    {},
                ),
                sort_columns,
            ),
        )

    def test_model_results_by_multi_filter(self):
        self.assertListEqual(
            [x1_A1_01A1, x1_A1_02A1, x1_A1_03A1],
            multikeysort(
                results(json_db, {"f1": "x", "f2": "1"}), sort_columns
            ),
        )
        self.assertListEqual(
            [x2_B1_01B1, x2_B1_02B1, x2_B1_03B1],
            multikeysort(
                results(json_db, {"f1": "x", "f2": "2"}), sort_columns
            ),
        )
        self.assertListEqual(
            [y2_A2_01A2, y2_A2_02A2],
            multikeysort(
                results(json_db, {"f2": "2", "art": "A"}), sort_columns
            ),
        )
        self.assertListEqual(
            [y2_A2_01A2, y2_A2_02A2, y3_A3_01A3, y3_A3_02A3],
            multikeysort(
                results(json_db, {"f1": "y", "art": "A"}), sort_columns
            ),
        )
        self.assertListEqual(
            [y3_A3_01A3, y3_A3_02A3],
            multikeysort(
                results(json_db, {"f1": "y", "art": "A", "alb": "A3"}),
                sort_columns,
            ),
        )
        self.assertListEqual(
            [y3_A3_02A3],
            multikeysort(
                results(
                    json_db, {"f1": "y", "art": "A", "alb": "A3", "trk": "02A3"}
                ),
                sort_columns,
            ),
        )

    def test_model_results_by_f1(self):
        self.assertListEqual(
            [
                x1_A1_01A1,
                x1_A1_02A1,
                x1_A1_03A1,
                x2_B1_01B1,
                x2_B1_02B1,
                x2_B1_03B1,
            ],
            multikeysort(results(json_db, {"f1": "x"}), sort_columns),
        )
        self.assertListEqual(
            [
                y2_A2_01A2,
                y2_A2_02A2,
                y3_A3_01A3,
                y3_A3_02A3,
                y4_B2_01B2,
                y4_B2_02B2,
            ],
            multikeysort(results(json_db, {"f1": "y"}), sort_columns),
        )

    def test_model_results_by_f2(self):
        self.assertListEqual(
            [x1_A1_01A1, x1_A1_02A1, x1_A1_03A1],
            multikeysort(results(json_db, {"f2": "1"}), sort_columns),
        )
        self.assertListEqual(
            [y2_A2_01A2, y2_A2_02A2, x2_B1_01B1, x2_B1_02B1, x2_B1_03B1],
            multikeysort(results(json_db, {"f2": "2"}), sort_columns),
        )
        self.assertListEqual(
            [y3_A3_01A3, y3_A3_02A3],
            multikeysort(results(json_db, {"f2": "3"}), sort_columns),
        )
        self.assertListEqual(
            [y4_B2_01B2, y4_B2_02B2],
            multikeysort(results(json_db, {"f2": "4"}), sort_columns),
        )

    def test_model_results_by_art(self):
        self.assertListEqual(
            [
                x1_A1_01A1,
                x1_A1_02A1,
                x1_A1_03A1,
                y2_A2_01A2,
                y2_A2_02A2,
                y3_A3_01A3,
                y3_A3_02A3,
            ],
            multikeysort(results(json_db, {"art": "A"}), sort_columns),
        )
        self.assertListEqual(
            [x2_B1_01B1, x2_B1_02B1, x2_B1_03B1, y4_B2_01B2, y4_B2_02B2],
            multikeysort(results(json_db, {"art": "B"}), sort_columns),
        )

    def test_model_results_by_alb(self):
        self.assertListEqual(
            [x1_A1_01A1, x1_A1_02A1, x1_A1_03A1],
            multikeysort(results(json_db, {"alb": "A1"}), sort_columns),
        )
        self.assertListEqual(
            [y2_A2_01A2, y2_A2_02A2],
            multikeysort(results(json_db, {"alb": "A2"}), sort_columns),
        )
        self.assertListEqual(
            [y3_A3_01A3, y3_A3_02A3],
            multikeysort(results(json_db, {"alb": "A3"}), sort_columns),
        )
        self.assertListEqual(
            [x2_B1_01B1, x2_B1_02B1, x2_B1_03B1],
            multikeysort(results(json_db, {"alb": "B1"}), sort_columns),
        )
        self.assertListEqual(
            [y4_B2_01B2, y4_B2_02B2],
            multikeysort(results(json_db, {"alb": "B2"}), sort_columns),
        )

    def test_model_results_by_trk(self):
        self.assertListEqual(
            [x1_A1_01A1],
            multikeysort(results(json_db, {"trk": "01A1"}), sort_columns),
        )
        self.assertListEqual(
            [y2_A2_01A2],
            multikeysort(results(json_db, {"trk": "01A2"}), sort_columns),
        )
        self.assertListEqual(
            [y3_A3_02A3],
            multikeysort(results(json_db, {"trk": "02A3"}), sort_columns),
        )
        self.assertListEqual(
            [x2_B1_02B1],
            multikeysort(results(json_db, {"trk": "02B1"}), sort_columns),
        )
        self.assertListEqual(
            [y4_B2_02B2],
            multikeysort(results(json_db, {"trk": "02B2"}), sort_columns),
        )


class TestModelIsInDb(unittest.TestCase):
    def test_model_is_in_db_filters(self):
        self.assertTrue(
            is_in_db(
                [x1_A1_01A1, x2_B1_01B1, y4_B2_01B2], {"f1": "x", "f2": "1"}
            )
        )
        self.assertFalse(
            is_in_db(
                [x1_A1_01A1, y2_A2_02A2, y4_B2_01B2], {"f1": "x", "f2": "4"}
            )
        )
        self.assertFalse(
            is_in_db(
                [y2_A2_01A2, y2_A2_02A2, y4_B2_01B2], {"f1": "x", "f2": "2"}
            )
        )
        self.assertFalse(is_in_db([], {"f1": "x", "f2": "2"}))

    def test_model_is_no_filter(self):
        self.assertFalse(is_in_db([y2_A2_01A2, y2_A2_02A2, y4_B2_01B2], {}))

    def test_model_is_empty(self):
        self.assertFalse(is_in_db([], {"f1": "x"}))
        self.assertFalse(is_in_db([], {}))


class TestModelRemove(unittest.TestCase):
    def test_model_remove_by_no_active_filter(self):
        self.assertListEqual(
            [
                x1_A1_01A1,
                x1_A1_02A1,
                x1_A1_03A1,
                y2_A2_01A2,
                y2_A2_02A2,
                y3_A3_01A3,
                y3_A3_02A3,
                x2_B1_01B1,
                x2_B1_02B1,
                x2_B1_03B1,
                y4_B2_01B2,
                y4_B2_02B2,
            ],
            multikeysort(remove(json_db, {}), sort_columns),
        )

    def test_model_remove_empty(self):
        self.assertListEqual(
            [], multikeysort(remove([], {"f1": "x"}), sort_columns)
        )
        self.assertListEqual([], multikeysort(remove([], {}), sort_columns))

    def test_model_remove_by_filter(self):
        self.assertListEqual(
            [
                y2_A2_01A2,
                y2_A2_02A2,
                y3_A3_01A3,
                y3_A3_02A3,
                y4_B2_01B2,
                y4_B2_02B2,
            ],
            multikeysort(remove(json_db, {"f1": "x"}), sort_columns),
        )
        self.assertListEqual(
            [
                x1_A1_01A1,
                x1_A1_02A1,
                x1_A1_03A1,
                x2_B1_01B1,
                x2_B1_02B1,
                x2_B1_03B1,
            ],
            multikeysort(remove(json_db, {"f1": "y"}), sort_columns),
        )
        self.assertListEqual(
            [
                x1_A1_01A1,
                x1_A1_02A1,
                x1_A1_03A1,
                y3_A3_01A3,
                y3_A3_02A3,
                x2_B1_01B1,
                x2_B1_02B1,
                x2_B1_03B1,
                y4_B2_01B2,
                y4_B2_02B2,
            ],
            multikeysort(remove(json_db, {"f1": "y", "f2": "2"}), sort_columns),
        )
        self.assertListEqual(
            [
                x1_A1_01A1,
                x1_A1_02A1,
                x1_A1_03A1,
                y2_A2_01A2,
                y2_A2_02A2,
                y3_A3_01A3,
                y3_A3_02A3,
                y4_B2_01B2,
                y4_B2_02B2,
            ],
            multikeysort(
                remove(json_db, {"art": "B", "alb": "B1"}), sort_columns
            ),
        )
        self.assertListEqual(
            [
                x1_A1_01A1,
                x1_A1_02A1,
                x1_A1_03A1,
                y2_A2_01A2,
                y2_A2_02A2,
                y3_A3_01A3,
                y3_A3_02A3,
                x2_B1_01B1,
                x2_B1_02B1,
                x2_B1_03B1,
                y4_B2_01B2,
                y4_B2_02B2,
            ],
            multikeysort(
                remove(json_db, {"art": "A", "alb": "B1"}), sort_columns
            ),
        )
