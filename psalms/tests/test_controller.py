# -*- coding: utf-8 -*-
from psalms.controller import get_bucket_db, bucket_toggle, navigator
from psalms.model import is_in_db
from psalms.tests.cases import BucketCase, full_results


class TestControllerNavigator(BucketCase):
    def test_controller_is_in_db(self):
        self.assertTrue(is_in_db(full_results, {"album": "fluidity"}))
        self.assertTrue(is_in_db(full_results, {"album": "peck"}))
        self.assertTrue(is_in_db(full_results, {"album": "sulky"}))
        self.assertTrue(is_in_db(full_results, {"album": "untitled"}))
        self.assertFalse(is_in_db(full_results, {"album": "notindb"}))

    def test_controller_get_bucket_db(self):
        self.assertListEqual([], get_bucket_db("bucket1"))
        self.assertListEqual([], get_bucket_db("bucket2"))
        self.assertListEqual([], get_bucket_db("bucket3"))
        all_db = get_bucket_db("all")
        self.assertEqual(
            set(["1998", "2000", "2001"]), set([l["year"] for l in all_db])
        )
        self.assertEqual(
            set(["acidjazz", "classical", "jazz"]),
            set([l["genre"] for l in all_db]),
        )
        self.assertEqual(
            set(["colaboy", "elioway", "tim bushell"]),
            set([l["artist"] for l in all_db]),
        )
        self.assertEqual(
            set(["fluidity", "peck", "sulky", "untitled"]),
            set([l["album"] for l in all_db]),
        )
        self.assertEqual(8, len(all_db))

    def test_controller_navigator_all(self):
        sidebar, results = navigator("all")
        self.assertDictEqual(
            {
                "year": ["1998", "2000", "2001"],
                "genre": ["acidjazz", "classical", "jazz"],
            },
            sidebar,
        )
        self.assertEqual(
            set(["1998", "2000", "2001"]), set([l["year"] for l in results])
        )
        self.assertEqual(
            set(["acidjazz", "classical", "jazz"]),
            set([l["genre"] for l in results]),
        )
        self.assertEqual(
            set(["colaboy", "elioway", "tim bushell"]),
            set([l["artist"] for l in results]),
        )
        self.assertEqual(
            set(["fluidity", "peck", "sulky", "untitled"]),
            set([l["album"] for l in results]),
        )
        self.assertEqual(8, len(results))
        self.assertListEqual(full_results, results)

    def test_controller_navigator_in_bucket(self):
        bucket_toggle("bucket1", "untitled")
        sidebar, results = navigator("bucket1")
        self.assertDictEqual(
            {"year": ["2000"], "genre": ["classical"]}, sidebar
        )
        self.assertEqual(1, len(results))
        self.assertListEqual(
            [
                {
                    "art": "2000/classical/tim%20bushell/untitled/folder.jpg",
                    "url": "2000/classical/tim%20bushell/untitled/01%20-%20untitled.mp3",
                    "year": "2000",
                    "genre": "classical",
                    "artist": "tim bushell",
                    "album": "untitled",
                    "name": "01 - untitled",
                }
            ],
            results,
        )
        # Adds another album to the bucket
        bucket_toggle("bucket1", "peck")
        sidebar, results = navigator("bucket1")
        self.assertDictEqual(
            {"year": ["1998", "2000"], "genre": ["classical", "jazz"]}, sidebar
        )
        self.assertEqual(3, len(results))
        self.assertListEqual(
            [
                {
                    "art": "1998/jazz/colaboy/peck/folder.jpg",
                    "url": "1998/jazz/colaboy/peck/01%20-%20pecks%20theme.mp3",
                    "year": "1998",
                    "genre": "jazz",
                    "artist": "colaboy",
                    "album": "peck",
                    "name": "01 - pecks theme",
                },
                {
                    "art": "1998/jazz/colaboy/peck/folder.jpg",
                    "url": "1998/jazz/colaboy/peck/02%20-%20pecks%20reprise.mp3",
                    "year": "1998",
                    "genre": "jazz",
                    "artist": "colaboy",
                    "album": "peck",
                    "name": "02 - pecks reprise",
                },
                {
                    "art": "2000/classical/tim%20bushell/untitled/folder.jpg",
                    "url": "2000/classical/tim%20bushell/untitled/01%20-%20untitled.mp3",
                    "year": "2000",
                    "genre": "classical",
                    "artist": "tim bushell",
                    "album": "untitled",
                    "name": "01 - untitled",
                },
            ],
            results,
        )

    def test_controller_navigator_outof_bucket(self):
        sidebar, results = navigator("bucket2")
        self.assertDictEqual({}, sidebar)
        self.assertListEqual([], results)


class TestControllerBucketToggle(BucketCase):
    def test_controller_bucket_toggle(self):
        # ADDING
        # BUCKET 1
        # Album in "all" bucket
        self.assertTrue(is_in_db(get_bucket_db("all"), {"album": "sulky"}))
        # Adds to bucket1 and reports it as in the list
        self.assertListEqual(["bucket1"], bucket_toggle("bucket1", "sulky"))
        # Album in bucket1
        self.assertTrue(is_in_db(get_bucket_db("bucket1"), {"album": "sulky"}))
        # No longer in "all" bucket
        self.assertFalse(is_in_db(get_bucket_db("all"), {"album": "sulky"}))
        # BUCKET 2
        # Adds to bucket1 and reports it as in the list
        self.assertListEqual(
            ["bucket1", "bucket2"], bucket_toggle("bucket2", "sulky")
        )
        # Album in bucket2
        self.assertTrue(is_in_db(get_bucket_db("bucket2"), {"album": "sulky"}))
        # Album in bucket1
        self.assertTrue(is_in_db(get_bucket_db("bucket1"), {"album": "sulky"}))
        # Not in "all" bucket
        self.assertFalse(is_in_db(get_bucket_db("all"), {"album": "sulky"}))
        # BUCKET 3
        # Only returns app supported buckets
        self.assertListEqual(
            ["bucket1", "bucket2"], bucket_toggle("bucket3", "sulky")
        )
        # Album not in bucket3
        self.assertFalse(is_in_db(get_bucket_db("bucket3"), {"album": "sulky"}))
        # Album in bucket1
        self.assertTrue(is_in_db(get_bucket_db("bucket1"), {"album": "sulky"}))
        # Album in bucket2
        self.assertTrue(is_in_db(get_bucket_db("bucket2"), {"album": "sulky"}))
        # Not in "all" bucket
        self.assertFalse(is_in_db(get_bucket_db("all"), {"album": "sulky"}))
        # REMOVING
        # BUCKET 1
        self.assertListEqual(["bucket2"], bucket_toggle("bucket1", "sulky"))
        # Album not in bucket1
        self.assertFalse(is_in_db(get_bucket_db("bucket1"), {"album": "sulky"}))
        # Album in bucket2
        self.assertTrue(is_in_db(get_bucket_db("bucket2"), {"album": "sulky"}))
        # Back in "all" bucket
        self.assertFalse(is_in_db(get_bucket_db("all"), {"album": "sulky"}))
        # BUCKET 2
        self.assertListEqual([], bucket_toggle("bucket2", "sulky"))
        # Album not in bucket2
        self.assertFalse(is_in_db(get_bucket_db("bucket2"), {"album": "sulky"}))
        # Album not in bucket1
        self.assertFalse(is_in_db(get_bucket_db("bucket1"), {"album": "sulky"}))
        # Back in "all" bucket
        self.assertTrue(is_in_db(get_bucket_db("all"), {"album": "sulky"}))

    def test_controller_bucket_toggle_allows_empty_alldb(self):
        # "all" bucket is not empty
        self.assertTrue(get_bucket_db("all"))
        # Move all albums to bucket1
        for album in ["fluidity", "peck", "sulky", "untitled"]:
            bucket_toggle("bucket1", album)
        # The "all" bucket is allowed to be empty.
        self.assertFalse(get_bucket_db("all"))
        # Empty all buckets
        self.tearDown()
        # The "all" bucket fills with empty buckets
        self.assertTrue(get_bucket_db("all"))
