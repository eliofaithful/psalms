# -*- coding: utf-8 -*-
import os
from urllib.parse import quote
from psalms.utils import filter_and, filter_or


def all_music(mp3s_path, pathways):
    """Return a full list of mp3 files."""
    json_db = []
    for root, dirs, files in os.walk(mp3s_path):
        for mp3file in files:
            if mp3file.endswith(".mp3"):
                json_db.append(mp3_model(os.path.join(root, mp3file), pathways))
    return json_db


def is_in_db(json_db, filters={}):
    if not filters:
        return False
    filtered_tracks = []
    for mp3_model in json_db:
        if filter_and(mp3_model, filters):
            return True
    return False


def mp3_model(mp3file_path, pathways):
    """Return a model object of the MP3 file."""
    rtn = dict()
    # Split the mp3 file path and list the portion after static/mp3s path.
    path_split = mp3file_path.split("/")
    pathways_split = pathways.split("/")
    # Extract the path relative to `static/mp3s`: Working backwards.
    # e.g.: if path: a/b/c/d/e/f and pathway: D/E/F then
    #             path_split[-3:] == d/e/f
    relative_path = len(pathways_split) * -1
    track_path_list = path_split[relative_path:]
    # Add the relative `static/{PSALMS_USER}` path of the MP3 file and it's artwork.
    rtn["art"] = quote(os.path.join(*track_path_list[0:-1], "folder.jpg"))
    rtn["url"] = quote(os.path.join(*track_path_list))
    # Dynamically build a model based on mp3 collection's directory structure.
    # i.e.: Break the mp3's path up into named units using the pathways.
    for pathway in pathways_split:
        rtn[pathway] = track_path_list[pathways_split.index(pathway)]
        # Remove "mp3" from the name.
        if pathway == "name":
            rtn[pathway] = os.path.splitext(rtn[pathway])[0]
    return rtn


def remove(json_db, filters={}):
    if not filters:
        return json_db
    filtered_tracks = []
    for mp3_model in json_db:
        if not filter_and(mp3_model, filters):
            filtered_tracks.append(mp3_model.copy())
    return filtered_tracks


def results(json_db, filters={}):
    if not filters:
        return json_db
    filtered_tracks = []
    for mp3_model in json_db:
        if filter_and(mp3_model, filters):
            filtered_tracks.append(mp3_model.copy())
    return filtered_tracks


def sidebar(json_db, pathways, sidebar_filtering_above):
    """Return a list of filters for the sidebar."""
    sidebar_links = {}
    # Establish which fields are going in the sidebar from pathways.
    pathways_split = pathways.split("/")
    sidebar_filtering_index = pathways_split.index(sidebar_filtering_above)
    sidebar_paths = pathways_split[:sidebar_filtering_index]
    # Get all the links from the database.
    for mp3_model in json_db:
        # For each field we're filtering by
        for sidebar_path in sidebar_paths:
            # Get the links we found so far
            links = sidebar_links.get(sidebar_path, [])
            # Append the link from this mp3_model
            links.append(mp3_model[sidebar_path])
            # Assign links to the field.
            sidebar_links[sidebar_path] = links
    for sidebar_path, links in sidebar_links.items():
        # Resolve each to a sorted unique list.
        sidebar_links[sidebar_path] = sorted(list(set(links)))
    return sidebar_links
