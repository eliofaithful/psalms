# -*- coding: utf-8 -*-
import flask, flask.views
import functools
import os
from psalms.controller import get_album_buckets, bucket_toggle, navigator


def login_required(method):
    @functools.wraps(method)
    def wrapper(*args, **kwargs):
        if "username" in flask.session:
            return method(*args, **kwargs)
        else:
            flask.flash("A login is required to see the page!")
            return flask.redirect(flask.url_for("index"))

    return wrapper


class Main(flask.views.MethodView):
    def get(self):
        BUCKETS = flask.current_app.config["BUCKETS"]
        return flask.render_template(
            "index.html", buckets=BUCKETS.split(","), bucket="all"
        )

    def post(self):
        USERS = {
            flask.current_app.config["PSALMS_USER"]: flask.current_app.config[
                "PSALMS_PASS"
            ]
        }
        if "logout" in flask.request.form:
            flask.session.pop("username", None)
            return flask.redirect(flask.url_for("index"))
        required = ["username", "passwd"]
        for r in required:
            if r not in flask.request.form:
                flask.flash("Error: {0} is required.".format(r))
                return flask.redirect(flask.url_for("index"))
        username = flask.request.form["username"]
        passwd = flask.request.form["passwd"]
        if username in USERS.keys() and USERS[username] == passwd:
            flask.session["username"] = username
        else:
            flask.flash("Username doesn't exist or incorrect password")
        return flask.redirect(flask.url_for("index"))


class Music(flask.views.MethodView):
    @login_required
    def get(self):
        BUCKETS = flask.current_app.config["BUCKETS"]
        # existing query string as a dictionary
        bucket = flask.request.url.split("/")[3]
        existing_qs_as_dict = flask.request.args.to_dict()
        sidebar, results = navigator(bucket, existing_qs_as_dict)
        return flask.render_template(
            "music.html",
            buckets=BUCKETS.split(","),
            bucket=bucket,
            sidebar=sidebar,
            results=results,
            existing_qs=existing_qs_as_dict,
        )


class Buckets(flask.views.MethodView):
    @login_required
    def get(self):
        album = flask.request.args["album"]
        album_in_buckets = get_album_buckets(album)
        return flask.jsonify(buckets=album_in_buckets)

    @login_required
    def post(self):
        bucket = flask.request.form["bucket"]
        album = flask.request.form["album"]
        album_in_buckets = bucket_toggle(bucket, album)
        return flask.jsonify(buckets=album_in_buckets)
