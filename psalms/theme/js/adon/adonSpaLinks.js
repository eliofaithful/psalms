jQuery.fn.extend({
  adonSpaLinks: function() {
    return $(this).each(function() {
      var tag = $(this)
      var tag_href = tag.prop('href')
      tag.prop('href', 'javascript:null;')
      tag.click(function() {
        location.href = tag_href
        return false
      })
    })
  },
})
