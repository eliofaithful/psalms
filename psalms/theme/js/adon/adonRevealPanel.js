jQuery.fn.extend({
  adonRevealPanel: function(btnId) {
    return $(this).each(function() {
      var tag = $(this)
      var tag_name = tag.prop('tagName')
      tag.addClass('adonRevealPanel')
      var BTN = $(btnId)
      BTN.click(function() {
        tag.toggleClass('slavevisible')
        BTN.toggleClass('slavevisible')
      })
      return tag
    })
  },
})
