![](https://elioway.gitlab.io/eliofaithful/psalms/elio-psalms-logo.png)

> Hear this, all ye people; give ear, all ye inhabitants of the world **Psalm 49, the elioWay**

# psalms ![alpha](https://elioway.gitlab.io/static/alpha.png "alpha")

Move to **timitee**. This is not fully faithful!

**psalms** is a web-based, MP3 player for a personal music collection.

- [psalms Documentation](https://elioway.gitlab.io/eliofaithful/psalms)
- [psalms Demo](https://elioway.gitlab.io/eliofaithful/psalms/demo.html)

## Installing

- [Installing psalms](https://elioway.gitlab.io/eliofaithful/god/installing.html)

## Seeing is believing

```shell
git clone https://gitlab.com/eliofaithful/psalms.git
cd psalms
virtualenv --python=python3 venv-psalms
# or
python3 -m venv venv-psalms

source venv-psalms/bin/activate
# or
source venv-psalms/bin/activate.fish
pip install -r requirements/local.txt
./run.py
```

- USER: psalms
- PASS: letmein

Navigate to [psalms](http://localhost:5000)

## Nutshell

### `./run.py`

### `pytest`

- [psalms Quickstart](https://elioway.gitlab.io/eliofaithful/psalms/quickstart.html)
- [psalms Credits](https://elioway.gitlab.io/eliofaithful/psalms/credits.html)

![](https://elioway.gitlab.io/eliofaithful/psalms/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:tcbushell@gmail.com)
